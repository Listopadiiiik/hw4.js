// Теоретичні питання.
// 1. Функції містять в собі послідовності команд. І щоб кілька разів не 
// переписувати ці команди (і викликати кілька разів), ми їх просто 
// розміщуємо у функції і викликаємо саме функцію.

// 2. Аргумент функції це та ж сама змінна, яку ми оголошуємо 
// у дужках (), і який належить тільки даній функції.

// 3. return повертає значення функції. Приклад:
// function numbers(a = 100, b = 170){
//     return a + b;
// }
// console.log(numbers()); // У консолі покаже 270.


// Завдання.

const calc = (num1, num2, sign) => {
    num1 = +prompt("Enter first number");
    if (isNaN(num1) || num1 === null){
        alert("Try next time");
    }
    num2 = +prompt("Write second number");
    if (isNaN(num2) || num2 === null){
        alert("Try again");
    }
    sign = prompt("Choose + or - or * or /");
switch (sign) {
    case '+':
        return num1 + num2;
    case '-':
        return num1 - num2;
    case '*':
        return num1 * num2;
    case '/':
        return num1 / num2;
    default:
        return ("You have only 4 choices...");
}
}
console.log(calc());
